<?php
/**
 * Configuration data for AdamTalk scripts
 *
 * PHP Version >=5.1.6
 *
 * @package    IATSTUTI
 * @subpackage AdamTalkCalls
 * @copyright  2010 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
/**#@+
 * Script constants
 */
/**
 * Script base URL/directory
 */
define( 'BASEURL', '' );
define( 'BASEDIR', dirname( __FILE__ ) );

/**#@+
 * Template directories
 */
define( 'TEMPLATE_BASE_DIR', dirname( __FILE__ ) . '/templates' );
define( 'TEMPLATE_RESOURCE_DIR', '' );

/**
 * Database connection string
 */
define( 'DB_DSN', 'mysql:dbname=;host=;port=;' );

/**
 * Database user
 */
define( 'DB_USER', '' );

/**
 * Database password
 */
define( 'DB_PASS', '' );

/**
 * Path and filename to store the Members Area session data
 */
define( 'COOKIEJAR', '' );

/**
 * Members Area login data
 */
define( 'MEMBERS_USERNAME', '' );
define( 'MEMBERS_PASSWORD', '' );

/**
 * Google Charts API
 */
define( 'GOOGLE_CHARTS_API', 'http://chart.apis.google.com/chart?' );
define( 'CHART_WIDTH', 400 );
define( 'CHART_HEIGHT', 250 );
define( 'CHART_TITLE_SIZE', 14 );
/**#@-*/

/**
 * All call type, we use this to tally call costs and counts
 */
define( 'CALL_TYPE_ALL', 1 );

try {
    $dbh = new PDO( DB_DSN, DB_USER, DB_PASS );
    $dbh->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
} catch ( PDOException $e ) {
    printf( 'Database Error: %s<br />', $e->getMessage() );
}

// Array of IP addresses that will allow you to view unobfuscated call numbers
$full_access = array();
