<?php
ini_set( 'display_errors', 1 );
error_reporting( E_ALL );
/**
 * Adam Talk usage data, by invoice
 *
 * PHP Version >=5.1.6
 *
 * @package    IATSTUTI
 * @subpackage Usage
 * @copyright  2011 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
require_once dirname( __FILE__ ) . '/config.php';
require_once BASEDIR . '/smarty/Smarty.class.php';

try {
    $tpl = new Smarty();
    $tpl->setTemplateDir( TEMPLATE_BASE_DIR );
    $tpl->setCompileDir( TEMPLATE_RESOURCE_DIR . '/templates_c/adamtalk' );
    $tpl->setConfigDir( TEMPLATE_RESOURCE_DIR . '/configs/adamtalk' );
    $tpl->setCacheDir( TEMPLATE_RESOURCE_DIR . '/cache/adamtalk' );
} catch ( SmartyException $e ) {
    printf( 'Smarty Error: %s<br />', $e->getMessage() );
    die();
}

$tpl->display( 'header.tpl' );

// Retrieve all invoices
$query = "SELECT `invoiceID`, `invoiceNumber` FROM `adamTalkInvoices`";
$result = $dbh->query( $query );
$invoice_result = $result->fetchAll( PDO::FETCH_ASSOC );

$invoices['unbilled'] = 'Unbilled Calls';

foreach ( $invoice_result as $invoice ) {
    $invoices[$invoice['invoiceID']] = $invoice['invoiceNumber'];
}

$tpl->assign( 'invoices', $invoices );

if ( isset( $_GET['invoice'] ) && is_numeric( $_GET['invoice'] ) ) {
    if ( isset( $_GET['invoice'] ) && isset( $invoices[$_GET['invoice']-1] ) ) {
        $tpl->assign( 'prev_invoice', $_GET['invoice']-1 );
    }

    if ( isset( $_GET['invoice'] ) && isset( $invoices[$_GET['invoice']+1] ) ) {
        $tpl->assign( 'next_invoice', $_GET['invoice']+1 );
    }
}

$tpl->display( 'menu.tpl' );

// We want data
if ( isset( $_GET['invoice'] ) ) {
    // Check invoice is valid
    if ( isset( $invoices[$_GET['invoice']] ) ) {
        try {
            if ( $_GET['invoice'] == 'unbilled' ) {
                // Do a live-data search from the Members Services Area
                $data_result = retrieve_unbilled_calls();
            } else {
                // Grab data for the given invoice
                $query = "  SELECT      `username`,
                                        `callTypeDescription`,
                                        `labelClass`,
                                        `startTime`,
                                        `duration`,
                                        `cost`,
                                        `adamTalkCallID`,
                                        `callTo`,
                                        `durationUnit`,
                                        `costUnit`
                            FROM        `adamTalkData`
                            LEFT JOIN   `adamTalkCallTypes`
                            ON          `adamTalkData`.`callTypeID` = `adamTalkCallTypes`.`callTypeID`
                            WHERE       `invoiceID` = ?";
                $sth = $dbh->prepare($query);
                $sth->execute( array( $_GET['invoice'], ) );
                $data_result = $sth->fetchAll( PDO::FETCH_ASSOC );
            }

            $total_seconds = $total_cents = 0;
            $call_groups   = array();

            foreach ( $data_result as $key => $data ) {
                $total_seconds += $data['duration'];
                $total_cents   += $data['cost']/100;

                // Instantiate or increment call type data
                if ( !isset( $call_groups[$data['callTypeDescription']] ) ) {
                    $call_groups[$data['callTypeDescription']]['count'] = 1;
                    $call_groups[$data['callTypeDescription']]['duration'] = $data['duration'];
                    $call_groups[$data['callTypeDescription']]['cost'] = $data['cost']/100;
                } else {
                    $call_groups[$data['callTypeDescription']]['count']++;
                    $call_groups[$data['callTypeDescription']]['duration'] += $data['duration'];
                    $call_groups[$data['callTypeDescription']]['cost'] += $data['cost']/100;
                }

                if ( !in_array( $_SERVER['REMOTE_ADDR'], $full_access ) ) {
                    $number_chunk = substr( $data_result[$key]['callTo'], 2, -3 );
                    $new_number_chunk = str_repeat( 'x', strlen( $number_chunk ) );
                    $data_result[$key]['callTo'] = str_replace(
                        $number_chunk,
                        $new_number_chunk,
                        $data_result[$key]['callTo']
                    );
                }

                $data_result[$key]['cost'] /= 100;
            }

            foreach ( $call_groups as $group => $data ) {
                $call_groups[$group]['duration'] = make_pretty_time( $call_groups[$group]['duration'] );
            }

            $tpl->assign( 'invoice_data', $data_result );
            $tpl->assign( 'total_time', make_pretty_time( $total_seconds, true ) );
            $tpl->assign( 'total_cents', $total_cents );
            $tpl->assign( 'call_groups', $call_groups );
            $tpl->display( 'invoice_data_display.tpl' );
        } catch ( PDOException $e ) {
            $tpl->assign( 'title', 'A Database Error Has Occurred' );
            $tpl->assign( 'class', 'error' );
            $tpl->assign( 'message', $e->getMessage() );
            $tpl->display( 'alert_message.tpl' );
        }
    } else {
        $tpl->assign( 'title', 'Invalid Invoice' );
        $tpl->assign( 'class', 'error' );
        $tpl->assign( 'message', 'The invoice you are trying to view does not exist. Please use the navigation menu.' );
        $tpl->display( 'alert_message.tpl' );
    }
}

$tpl->display( 'footer.tpl' );

/**
 * Convert seconds into hours, minutes, seconds formatted string
 *
 * @access public
 * @param int $seconds Seconds to convert
 * @param bool $title If true, format for display in titles
 * @return string
 */
function make_pretty_time( $seconds, $title = false )
{
    $time = null;

    if ( intval( $seconds / 3600 ) ) {
        if ( $title === true ) {
            $time .= sprintf( '%d <small>h</small> ', $seconds / 3600 );
        } else {
            $time .= sprintf( '%d h ', $seconds / 3600 );
        }
    }

    if ( ( $seconds / 60 ) % 60 ) {
        if ( $title === true ) {
            $time .= sprintf( '%d <small>m</small> ', ( $seconds / 60 ) % 60 );
        } else {
            $time .= sprintf( '%d m ', ( $seconds / 60 ) % 60 );
        }
    }

    if ( $seconds % 60 ) {
        if ( $title === true ) {
            $time .= sprintf( '%d <small>s</small>', $seconds % 60 );
        } else {
            $time .= sprintf( '%d s', $seconds % 60 );
        }
    }

    return $time;
}

/**
 * Login to Members Services Area to retrieve current call data
 *
 * @access public
 * @throws Exception on error
 * @return array
 */
function retrieve_unbilled_calls()
{
    global $dbh;

    $return = array();

    $getCallTypeID = $dbh->prepare(
        'SELECT `callTypeID` FROM `adamTalkCallTypes` WHERE `callTypeDescription` = ?'
    );

    $postFields = sprintf(
        'Login=Log%%20In&Username=%s&Password=%s',
        MEMBERS_USERNAME,
        MEMBERS_PASSWORD
    );

    // Login
    $ch = curl_init();
    curl_setopt_array(
        $ch,
        array(
            CURLOPT_URL             => 'https://members.adam.com.au/index.php',
            CURLOPT_POST            => true,
            CURLOPT_COOKIEFILE      => COOKIEJAR,
            CURLOPT_COOKIEJAR       => COOKIEJAR,
            CURLOPT_POSTFIELDS      => $postFields,
            CURLOPT_FOLLOWLOCATION  => true,
            CURLOPT_HEADER          => true,
            CURLOPT_RETURNTRANSFER  => true,
        )
    );

    $result = curl_exec( $ch );

    if ( strpos( $result, '200 OK' ) === false ) {
        throw new Exception( 'Could not login to Members Services Area' );
    } else {
        $exportURL = sprintf(
            'https://members.adam.com.au/voipmanager/exportCalls.php?f=%s-All.xml',
            MEMBERS_USERNAME
        );

        // Get XML call list
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL             => $exportURL,
                CURLOPT_POST            => true,
                CURLOPT_COOKIEFILE      => COOKIEJAR,
                CURLOPT_COOKIEJAR       => COOKIEJAR,
                CURLOPT_POSTFIELDS      => $postFields,
                CURLOPT_RETURNTRANSFER  => true,
                CURLOPT_FOLLOWLOCATION  => true,
            )
        );

        $result = simplexml_load_string( curl_exec( $ch ) );

        if ( count( (array)$result->AdamTalk->CallList ) == 0 ) {
            throw new Exception( 'No unbilled calls found' );
        }

        // Derive the username for this account
        $adamTalk = $result->AdamTalk->attributes();
        $username = $adamTalk['Username'];

        $unit_duration = $result->AdamTalk->Units->Duration;
        $unit_cost     = $result->AdamTalk->Units->Cost;

        foreach ( $result->AdamTalk->CallList->Category as $value ) {
            $categoryAttributes = $value->attributes();
            $callType = $categoryAttributes['Type'];

            $getCallTypeID->execute( array( $callType, ) );
            $callTypeID = $getCallTypeID->fetchAll( PDO::FETCH_ASSOC );

            foreach ( $value as $call ) {
                $callData = $call->attributes();

                $return[] = array(
                    'username'            => $username,
                    'callTypeDescription' => get_call_type_description( $callTypeID[0]['callTypeID'] ),
                    'labelClass'          => get_call_type_label_class( $callTypeID[0]['callTypeID'] ),
                    'startTime'           => date( 'Y-m-d H:i:s', strtotime( $callData['StartTime'] ) ),
                    'duration'            => $callData['Duration'],
                    'cost'                => $callData['Cost'],
                    'adamTalkCallID'      => $callData['CallID'],
                    'callTo'              => $call,
                    'durationUnit'        => $unit_duration,
                    'costUnit'            => $unit_cost,
                );
            }
        }
    }

    return $return;
}

/**
 * Get call type description for a given call type ID
 *
 * @access public
 * @param int $callTypeID Call type to retrieve description for
 * @throws Exception on database error
 * @return string
 */
function get_call_type_description( $callTypeID )
{
    global $dbh;

    try {
        $query = "SELECT `callTypeDescription` FROM `adamTalkCallTypes` WHERE `callTypeID` = ?";
        $sth = $dbh->prepare( $query );
        $sth->execute( array( $callTypeID, ) );

        $result = $sth->fetch( PDO::FETCH_ASSOC );
        return $result['callTypeDescription'];
    } catch ( PDOException $e ) {
        throw new Excepetion( $e->getMessage() );
    }
}

/**
 * Get call type label class for a given call type ID
 *
 * @access public
 * @param int $callTypeID Call type to retrieve description for
 * @throws Exception on database error
 * @return string
 */
function get_call_type_label_class( $callTypeID )
{
    global $dbh;

    try {
        $query = "SELECT `labelClass` FROM `adamTalkCallTypes` WHERE `callTypeID` = ?";
        $sth = $dbh->prepare( $query );
        $sth->execute( array( $callTypeID, ) );

        $result = $sth->fetch( PDO::FETCH_ASSOC );
        return $result['labelClass'];
    } catch ( PDOException $e ) {
        throw new Excepetion( $e->getMessage() );
    }
}
