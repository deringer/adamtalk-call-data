<?php
/**
 * Script to login to Adam Internet Members Services Area and retrieve the XML feed for the AdamTalk calls for a given
 * Invoice, then store them locally.
 *
 * The script, using cURL:
 * - Logs into the Adam Internet Members Services Area
 * - Stores the session cookie for the login
 * - Looks for the XML data for the given invoice number
 * - Stores the data in the specified database, if it exists
 *
 * PHP Version >=5.1.6
 *
 * @package    IATSTUTI
 * @subpackage AdamTalkCalls
 * @copyright  2010 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
require_once dirname( __FILE__ ) . '/config.php';

try {
    // Prepare database queries
    $adamTalkData = $dbh->prepare(
        '   INSERT INTO `adamTalkData`  (   `invoiceID`,
                                            `username`,
                                            `callTypeID`,
                                            `startTime`,
                                            `duration`,
                                            `cost`,
                                            `adamTalkCallID`,
                                            `callTo`,
                                            `durationUnit`,
                                            `costUnit`
                                        ) VALUE (
                                            ?,
                                            ?,
                                            ?,
                                            ?,
                                            ?,
                                            ?,
                                            ?,
                                            ?,
                                            ?,
                                            ?
                                        )'
    );

    $invoiceExists = $dbh->prepare(
        'SELECT `invoiceID` FROM `adamTalkInvoices` WHERE `invoiceNumber` = ?'
    );

    $addInvoice = $dbh->prepare(
        'INSERT INTO `adamTalkInvoices` ( `invoiceNumber` ) VALUES ( ? )'
    );

    $getCallTypeID = $dbh->prepare(
        'SELECT `callTypeID` FROM `adamTalkCallTypes` WHERE `callTypeDescription` = ?'
    );
} catch ( PDOException $e ) {
    print 'Database Error: ' . $e->getMessage();
}

print '<form name="getInvoice" method="post">';
print '<table cellspacing="4" cellpadding="1">';
print '<tr><th colspan="2">Obtain AdamTalk Call Data</th></tr>';
print '<tr><th><label for="invoiceID">Invoice #</th><td><input type="text" name="invoiceID" id="invoiceID"></td>';
print '<tr><td style="text-align: center;"><input type="submit" name="Get AdamTalk Data"></td></tr>';
print '</table><br />';

if ( isset( $_POST['invoiceID'] ) ) {
    try {
        // Check the invoice doesn't already exist
        $invoiceExists->execute( array( $_POST['invoiceID'], ) );

        if ( count( $invoiceExists->fetchAll( PDO::FETCH_ASSOC ) ) != 0 ) {
            die( "Invoice {$_POST['invoiceID']} already exists in the database\n" );
        }

        print "Logging into Adam Internet Members Services<br />";
        ob_flush();
        flush();

        $postFields = sprintf(
            'Login=Log%%20In&Username=%s&Password=%s',
            MEMBERS_USERNAME,
            MEMBERS_PASSWORD
        );

        // Login
        $ch = curl_init();
        curl_setopt_array(
            $ch,
            array(
                CURLOPT_URL             => 'https://members.adam.com.au/index.php',
                CURLOPT_POST            => true,
                CURLOPT_COOKIEFILE      => COOKIEJAR,
                CURLOPT_COOKIEJAR       => COOKIEJAR,
                CURLOPT_POSTFIELDS      => $postFields,
                CURLOPT_FOLLOWLOCATION  => true,
                CURLOPT_HEADER          => true,
                CURLOPT_RETURNTRANSFER  => true,
            )
        );

        $result = curl_exec( $ch );

        if ( strpos( $result, '200 OK' ) === false ) {
            print "Could not login<br />";
            ob_flush();
            flush();
        } else {
            print "Successfully logged in!<br />";
            ob_flush();
            flush();

            $exportURL = sprintf(
                'https://members.adam.com.au/voipmanager/exportCalls.php?invoiceID=%d&f=%s-All.xml',
                $_POST['invoiceID'],
                MEMBERS_USERNAME
            );

            print "Attempting to retrieve AdamTalk call data<br />";
            ob_flush();
            flush();

            // Get XML call list
            $ch = curl_init();
            curl_setopt_array(
                $ch,
                array(
                    CURLOPT_URL             => $exportURL,
                    CURLOPT_POST            => true,
                    CURLOPT_COOKIEFILE      => COOKIEJAR,
                    CURLOPT_COOKIEJAR       => COOKIEJAR,
                    CURLOPT_POSTFIELDS      => $postFields,
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_FOLLOWLOCATION  => true,
                )
            );

            print "Successfully retrieved AdamTalk call data<br />";
            ob_flush();
            flush();

            $result = simplexml_load_string( curl_exec( $ch ) );

            if ( count( (array)$result->AdamTalk->CallList ) == 0 ) {
                die( "No calls in XML for invoice {$_POST['invoiceID']}\n" );
            }

            print "Storing AdamTalk call data<br />";
            ob_flush();
            flush();

            $addInvoice->execute( array( $_POST['invoiceID'], ) );
            $invoiceID = $dbh->lastInsertId();

            print "Added Invoice ID {$invoiceID}<br />";
            ob_flush();
            flush();

            // Derive the username for this account
            $adamTalk = $result->AdamTalk->attributes();
            $username = $adamTalk['Username'];

            $duration = $result->AdamTalk->Units->Duration;
            $cost = $result->AdamTalk->Units->Cost;

            foreach ( $result->AdamTalk->CallList->Category as $value ) {
                $categoryAttributes = $value->attributes();
                $callType = $categoryAttributes['Type'];

                $getCallTypeID->execute( array( $callType, ) );
                $callTypeID = $getCallTypeID->fetchAll( PDO::FETCH_ASSOC );

                foreach ( $value as $call ) {
                    $callData = $call->attributes();

                    $adamTalkData->execute(
                        array(
                            $invoiceID,
                            $username,
                            $callTypeID[0]['callTypeID'],
                            date( 'Y-m-d H:i:s', strtotime( $callData['StartTime'] ) ),
                            $callData['Duration'],
                            $callData['Cost'],
                            $callData['CallID'],
                            $call,
                            $duration,
                            $cost,
                        )
                    );
                }
            }

            print "Done!";
            ob_flush();
            flush();
        }
    } catch ( PDOException $e ) {
        print 'Database Error: ' . $e->getMessage();
    }
}
