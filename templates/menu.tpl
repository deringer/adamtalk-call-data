<div class="row">
    <div class="span16">
    <ul class="tabs">
        <li class="dropdown" data-dropdown="dropdown">
            <a href="#" class="dropdown-toggle">Invoices</a>
            <ul class="dropdown-menu">
{foreach from=$invoices key=invoice item=invoice_number}
               <li{if isset( $smarty.get.invoice ) && $smarty.get.invoice eq $invoice} class="active"{/if}><a href="?invoice={$invoice}" title="View Invoice {$invoice_number}">{$invoice_number}</a></li>
{/foreach}
            </ul>
        </li>
{if isset( $prev_invoice )}
        <li><a href="?invoice={$prev_invoice}" title="View prev invoice">Previous Invoice</a></li>
{/if}
{if isset( $next_invoice )}
        <li><a href="?invoice={$next_invoice}" title="View next invoice">Next Invoice</a></li>
{/if}
    </ul>
    </div>
</div>
