{literal}
<script type="text/javascript">
{/literal}
    google.load( "visualization", "1", { packages: [ "corechart" ] } );
    google.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn( 'string', 'Call Type' );
        data.addColumn( 'number', 'Total Calls' );
        data.addRows( {$call_groups|@count} );
{foreach from=$call_groups key=group item=data}
        data.setValue( {$data@index}, 0, '{$group} calls' );
        data.setValue( {$data@index}, 1, {$data.count} );
{/foreach}

        var chart = new google.visualization.PieChart( document.getElementById( 'call_breakdown' ) );
        chart.draw( data, { width: 450, height: 400, legend: 'bottom', chartArea: { top: 10 } } );
    }
{literal}
</script>
{/literal}

<div class="row">
    <div class="span16">
        <h1>Viewing {if $smarty.get.invoice eq 'unbilled'}unbilled calls{else} calls from invoice #{$invoices[$smarty.get.invoice]}{/if}</h1>
    </div>
</div>

<!-- call summary -->
<div class="row">
    <div class="span8" id="call_breakdown"><h3>Call Breakdown</h3></div>
    <div class="span8">
        <div class="row">
            <div class="span4">
                <h2>{$invoice_data|@count} <small>total call{if $invoice_data|@count > 1}s{/if}</small></h2>
{foreach from=$call_groups key=group item=data}
{if $data@first}<hr />{/if}
                <div class="row">
                    <div class="span1"><h3>{$data.count}</h3></div>
                    <div class="span3"><h3><small>{$group} call{if $data.count > 1}s{/if}</small></h3></div>
                </div>
{/foreach}
            </div>
            <div class="span4">
                <h2>{$total_time}</h2>
{foreach from=$call_groups key=group item=data}
{if $data@first}<hr />{/if}
                <div class="row">
                    <div class="span2"><h3>{$group}</h3></div>
                    <div class="span2"><h3><small>{$data.duration}</small></h3></div>
                </div>
{/foreach}
            </div>
        </div>
        <div class="row"><hr /></div>
        <div class="row">
            <div class="span4">
                <h2>${$total_cents|string_format:"%.2f"} <small>invoice total</small></h2>
{foreach from=$call_groups key=group item=data}
{if $data@first}<hr />{/if}
                <div class="row">
                    <div class="span1"><h3>${$data.cost|string_format:"%.2f"}</h3></div>
                    <div class="span3"><h3><small>{$group} calls</small></h3></div>
                </div>
{/foreach}
            </div>
        </div>
    </div>
</div>
<!-- end call summary -->

<!-- call data -->
<div class="row">
    <div class="span16">
        <table class="condensed-table zebra-striped">
        <thead>
        <tr>
            <th>Username</th>
            <th>Type</th>
            <th>Call Time</th>
            <th>Duration</th>
            <th>Call ID</th>
            <th>Call To</th>
            <th>Call Cost</th>
        </tr>
        </thead>
        </tbody>
{foreach from=$invoice_data item=data}
        <tr>
            <td>{$data.username}</td>
            <td><span class="label{if $data.labelClass} {$data.labelClass}{/if}">{$data.callTypeDescription}</span></td>
            <td>{$data.startTime}</td>
            <td>{$data.duration|make_pretty_time}</td>
            <td>{$data.adamTalkCallID}</td>
            <td>{$data.callTo}</td>
            <td>${$data.cost|string_format:"%.2f"}</td>
        </tr>
{/foreach}
        </tbody>
        </table>
    </div>
</div>
