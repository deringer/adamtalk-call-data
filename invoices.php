<html>
<head>
<title>AdamTalk Call Usage Data</title>
<style>

body {
    font-family: Arial, Helvetica, sans-serif;
    text-size: 10px;
    color: #000000;
}

a:visited, a:active, a:link {
    text-decoration: none;
    border-bottom: 1px dotted #CC0000;
    color: #CC0000;
    font-size: 11px;
}

a:hover {
    color: #0000CC;
    border-bottom: 1px dotted #0000CC;
}

#usageGraph {
    position: absolute;
    margin: 0;
    top: 0;
    right: 20px;
}

</style>
</head>

<body>
<?php
/**
 * Script for listing out all available invoices for call data listing
 *
 * PHP Version >=5.1.6
 *
 * @package    IATSTUTI
 * @subpackage AdamTalkCalls
 * @copyright  2010 IATSTUTI
 * @author     Michael Dyrynda <michael@iatstuti.net>
 */
require_once dirname( __FILE__ ) . '/config.php';

/**
 * Instantiate variables
 */
$invoices = array();
$callTypes = array();

try {
    $result = $dbh->query( 'SELECT `invoiceID`, `invoiceNumber` FROM `adamTalkInvoices` ORDER BY `invoiceID` DESC' );
    $invoices = $result->fetchAll( PDO::FETCH_ASSOC );

    if ( count( $invoices ) == 0 ) {
        print 'No invoices to display<br />';
    } else {
        print '<table>';
        
        foreach ( $invoices as $invoice ) {
            printf(
                '<tr><td><a href="%s/invoices.php?invoiceID=%d" title="View Invoice %d">%d</a></td></tr>',
                BASEURL,
                $invoice['invoiceID'],
                $invoice['invoiceNumber'],
                $invoice['invoiceNumber']
            );
        }

        print '</table>';
    }
} catch ( PDOException $e ) {
    printf( 'Database Error: %s<br />', $e->getMessage() );
}

if ( isset( $_GET['invoiceID'] ) ) {
    // Grab all the call types
    $sth = $dbh->prepare(
        '   SELECT  `callTypeID`,
                    `callTypeDescription`,
                    `chartColor`
            FROM    `adamTalkCallTypes`'
    );

    $sth->execute();

    while ( $callType = $sth->fetch( PDO::FETCH_ASSOC ) ) {
        $description[$callType['callTypeID']] = $callType['callTypeDescription'];
        $callCount[$callType['callTypeID']] = 0;
        $color[$callType['callTypeID']] = $callType['chartColor'];
        $callCost[$callType['callTypeID']] = 0;
    }

    // Grab call data for this invoice
    $sth = $dbh->prepare(
        '   SELECT      `callTypeID`,
                        COUNT( `callTypeID` ) as `callCount`,
                        SUM( `cost` ) as `callCost`
            FROM        `adamTalkData`
            WHERE       `invoiceID` = ?
            GROUP BY    `callTypeID`'
    );

    $sth->execute( array( $_GET['invoiceID'] ) );
    $callData = $sth->fetchAll( PDO::FETCH_ASSOC );

    if ( count( $result ) == 0 ) {
        print 'No data to show';
    } else {
        $maxValue = 0;

        foreach ( $callData as $call ) {
            $callCount[$call['callTypeID']] = (int)$call['callCount'];
            $callCost[$call['callTypeID']]  = (int)$call['callCost'];

            // Keep a total
            $callCount[CALL_TYPE_ALL]      += (int)$call['callCount'];
            $callCost[CALL_TYPE_ALL]       += (int)$call['callCost'];

            $maxValue = max( $maxValue, $call['callCount'], $callCount[CALL_TYPE_ALL] );
        }

        // For some reason, Google Visualisation swaps the order around
        $description = array_reverse( $description );

        $calls = array_keys( $callCount );

        foreach ( $calls as $call ) {
            $callCount[$call] = $callCount[$call] / $maxValue * 100;
        }

        printf(
            '<img id="usageGraph" src="%scht=bhs&amp;chs=%dx%d&amp;chco=%s&amp;chd=t:%s&amp;chxl=1:|%s&amp;chxt=x,y&amp;chxr=0,0,%d&chtt=Invoice %d Breakdown&amp;chts=000000,%d" title="Invoice %d Chart">',
            GOOGLE_CHARTS_API,
            CHART_WIDTH,
            CHART_HEIGHT,
            join( '|', $color ),
            join( ',', $callCount ),
            join( '|', $description ),
            $maxValue,
            $_GET['invoiceID'],
            CHART_TITLE_SIZE,
            $_GET['invoiceID']
        );
    }
}
?>
</body>
</html>
