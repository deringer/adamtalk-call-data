CREATE TABLE IF NOT EXISTS `adamTalkCallTypes` (
  `callTypeID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `callTypeDescription` varchar(30) DEFAULT NULL,
  `chartColor` varchar(6) DEFAULT NULL,
  `labelClass` enum('success','warning','important','notice') DEFAULT NULL,
  PRIMARY KEY (`callTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 ;

INSERT INTO `adamTalkCallTypes` (`callTypeID`, `callTypeDescription`, `chartColor`, `labelClass`) VALUES
(1, 'All', 'CC0000', NULL),
(2, 'Local', '4D89F9', 'notice'),
(3, 'National', 'C6D9FD', 'notice'),
(4, 'Mobile', 'FFE7C6', 'important'),
(5, 'Special', '80C65A', 'warning'),
(6, 'Free', 'FF9900', 'success'),
(7, 'International', 'FFC6A5', 'important');

CREATE TABLE IF NOT EXISTS `adamTalkData` (
  `callID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceID` int(10) unsigned NOT NULL DEFAULT '0',
  `username` varchar(64) DEFAULT NULL,
  `callTypeID` int(10) unsigned NOT NULL DEFAULT '0',
  `startTime` datetime NOT NULL,
  `duration` int(11) NOT NULL DEFAULT '0',
  `cost` int(11) NOT NULL DEFAULT '0',
  `adamTalkCallID` varchar(128) DEFAULT NULL,
  `callTo` varchar(128) DEFAULT NULL,
  `durationUnit` varchar(20) DEFAULT NULL,
  `costUnit` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`callID`)
) ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS `adamTalkInvoices` (
  `invoiceID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoiceNumber` int(10) unsigned NOT NULL,
  PRIMARY KEY (`invoiceID`),
  KEY `invoiceNumber` (`invoiceNumber`)
) ENGINE=InnoDB;
